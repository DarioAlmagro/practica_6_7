package arapp.darioalmagro.facci.practica6;

import android.content.Context;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class VibradorActivity extends AppCompatActivity {

    Button btnVibrar;
    private Vibrator vibrator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vibrador);

        vibrator = (Vibrator)this.getSystemService(Context.VIBRATOR_SERVICE);
        btnVibrar = (Button) findViewById(R.id.btnVibrar);

        btnVibrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrator.vibrate(600);
            }
        });
    }
}
