package arapp.darioalmagro.facci.practica6;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SensoresActivity extends AppCompatActivity {

    Button btnVibrador, btnAceleremotro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensores);

        btnVibrador = (Button) findViewById(R.id.btnVibrador);
        btnAceleremotro = (Button) findViewById(R.id.btnAcelerometro);

        btnVibrador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SensoresActivity.this, VibradorActivity.class);
                startActivity(intent);
            }
        });
        btnAceleremotro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SensoresActivity.this, AcelerometroActivity.class);
                startActivity(intent);
            }
        });

    }
}
